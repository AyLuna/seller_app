import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:satyjy/widgets/simple_app_bar.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/home_screen.dart';
import '../modal/items.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/progress_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import '../global/global.dart';
import '../widgets/error_dialog.dart';
import 'package:firebase_storage/firebase_storage.dart' as storageRef;

import '../widgets/styled_text_widget.dart';

class ItemsUploadScreen extends StatefulWidget {
  final Items? model;

  const ItemsUploadScreen({Key? key, required this.model}) : super(key: key);

  @override
  State<ItemsUploadScreen> createState() => _ItemsUploadScreenState();
}

class _ItemsUploadScreenState extends State<ItemsUploadScreen> {

  String language="Eng";

  XFile? imageXFile;
  final ImagePicker _picker = ImagePicker();

  captureImageWithCamera()async{
    Navigator.pop(context);
    imageXFile= await _picker.pickImage(source: ImageSource.camera,
        maxHeight:720, maxWidth: 1280);
    setState(() {
      imageXFile;
    });
  }

  pickImageFromGallery()async{
    Navigator.pop(context);
    imageXFile= await _picker.pickImage(source: ImageSource.gallery,
        maxHeight:720, maxWidth: 1280);
    setState(() {
      imageXFile;
    });
  }

  takeImage(mContext){
    return showDialog(context: mContext, builder:(c){
      return SimpleDialog(
        title: Text(LocaleKeys.Item_Image.tr(), style: TextStyle(color: Colors.amber,fontWeight:FontWeight.bold ),),
        children: [
          SimpleDialogOption(
            child: Text(LocaleKeys.Capture_with_Camera.tr(), style: TextStyle(color: Colors.grey),),
            onPressed:captureImageWithCamera,
          ),
          SimpleDialogOption(
            child: Text(LocaleKeys.Select_from_Gallery.tr(), style: TextStyle(color: Colors.grey),),
            onPressed:pickImageFromGallery,
          ),
          SimpleDialogOption(
            child: Text(LocaleKeys.Cancel.tr(), style: TextStyle(color: Colors.red),),
            onPressed:(){
              Navigator.pop(context);
            },
          ),
        ],
      );
    });
  }


  TextEditingController titleController=TextEditingController();
  TextEditingController descriptionController=TextEditingController();
  TextEditingController priceController=TextEditingController();

  clearMenuUploadForm() {
    setState(() {
      titleController.clear();
      priceController.clear();
      descriptionController.clear();
      imageXFile==null;
    });
  }

  bool uploading=false;

  validateUploadForm() async{

    if(imageXFile!=null){
      if(titleController.text.isNotEmpty && descriptionController.text.isNotEmpty && priceController.text.isNotEmpty){


        setState(() {
          uploading=true;
        });

        String downloadUrl=await uploadImage(File(imageXFile!.path));

        saveInfoToFirebase(downloadUrl);

      }
      else{
        showDialog(context: context, builder:(c){
          return ErrorDialog(message: LocaleKeys.Please_add_Short_information_about_Item.tr(),);
        });
      }

    }
    else{
      showDialog(context: context, builder:(c){
        return ErrorDialog(message: LocaleKeys.Please_Add_picture_of_Item.tr(),);
      });
    }


  }

  String uniqueIdName=DateTime.now().microsecondsSinceEpoch.toString();

  uploadImage(mImageFile) async {

    storageRef.Reference reference=storageRef.FirebaseStorage.instance.ref().child("Items");
    storageRef.UploadTask uploadTask=reference.child(uniqueIdName + ".jpg").putFile(mImageFile);
    storageRef.TaskSnapshot taskSnapshot= await uploadTask.whenComplete((){});
    String downloadUrl=await taskSnapshot.ref.getDownloadURL();

    return downloadUrl;


  }

  saveInfoToFirebase(downloadUrl) {
    final ref = FirebaseFirestore.instance.collection("sellers").doc(sharedPreferences!.getString("uid")).collection("items");

    ref.doc(uniqueIdName).set({
      "itemID":uniqueIdName,
      "sellerUID":sharedPreferences!.getString("uid"),
      "sellerName":sharedPreferences!.getString("name"),
      "categoryEng":defaultCategoryEng,
      "longDescription":descriptionController.text.toString(),
      "price":int.parse(priceController.text),
      "title":titleController.text.toString(),
      "publishedDate":DateTime.now(),
      "thumbnailUrl":downloadUrl,
      "status":"available",

    }).then((value){
      final itemsRef = FirebaseFirestore.instance.collection("items");
      itemsRef.doc(uniqueIdName).set({
        "itemID":uniqueIdName,
        "sellerUID":sharedPreferences!.getString("uid"),
        "sellerName":sharedPreferences!.getString("name"),
        "categoryEng":defaultCategoryEng,
        "longDescription":descriptionController.text.toString(),
        "price":int.parse(priceController.text),
        "title":titleController.text.toString(),
        "publishedDate":DateTime.now(),
        "thumbnailUrl":downloadUrl,
        "status":"available",
      });

    }).then((value){
      clearMenuUploadForm();
      setState(() {
        uniqueIdName=DateTime.now().microsecondsSinceEpoch.toString();
        uploading=false;
      });
    });
  }



  defaultScreen(){
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        appBar: SimpleAppBar(title: LocaleKeys.Add_new_product.tr(),),
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(onTap: (){
                  takeImage(context);
                },
                    child: Container(
                      width: 300,
                      height: 70,
                      decoration: BoxDecoration(
                        color:Color.fromRGBO(240, 86, 52, 1),
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: Center(child: StyledTextWidget(text: LocaleKeys.Add_new_product.tr(),size: 20,color: Colors.white)),)),
              ],
            ),
          ),
        ),
      ),
    );
  }


  final categoryListEng=["Bags","Ceramics","Paintings","Beads","Embroidery","Dolls","Rugs","Knitwear","Pastry","Others"];
  String defaultCategoryEng="Bags";
  setProductCategoryEng() {
    return
      Container(
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical:4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20),
          border: Border.all(
              color: Colors.grey
          )
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          items: categoryListEng.map((e)=>DropdownMenuItem<String>(
            child: Text(e),value:e,
          )).toList(),
          value: defaultCategoryEng,
          iconSize: 36,
          icon: Icon(Icons.arrow_drop_down_sharp,color:Colors.black),
          isExpanded: true,
          onChanged: (value){
            setState(() {
              defaultCategoryEng=value.toString();
            });
          },

        ),
      ),
    );
  }


  itemsUploadFromScreen() {
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        appBar: AppBar(
          leading:IconButton(onPressed: (){
            clearMenuUploadForm();
            Navigator.push(context, MaterialPageRoute(builder: (c)=> HomeScreen()));
          }, icon: Icon(Icons.arrow_back_ios_new, size: 25, color:Color.fromRGBO(72, 72, 72, 1))),
          flexibleSpace: Container(
            color: Colors.white,
          ),
          centerTitle: true,
          title: StyledTextWidget(text: LocaleKeys.Upload_new_product.tr(),size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
        ),
        body: ListView(
          children: [
            uploading==true?linearProrgress():Text(""),
            Container(
              height: 230,
              width: MediaQuery.of(context).size.width*0.8,
              child: Center(
                child: AspectRatio(aspectRatio: 16/9,
                  child: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(image: FileImage(File(imageXFile!.path)),fit: BoxFit.cover),
                    ),
                  ),),
              ),
            ),
            setProductCategoryEng(),
            CustomTextField(
                  data: Icons.title,
                  controller: titleController,
                  hintText: LocaleKeys.Title.tr(),
                  isObsecre: false,
                ),
            CustomTextField(
                  data: Icons.description,
                  controller: descriptionController,
                  hintText: LocaleKeys.Short_Info.tr(),
                  isObsecre: false ,
                ),
            CustomTextField(
                  data: Icons.money,
                  controller: priceController,
                  hintText: LocaleKeys.Price.tr(),
                  isObsecre: false,
                ),


            InkWell(onTap: (){
              uploading? null: validateUploadForm();
            },
                child: Container(
                  width: 300,
                  height: 70,
                  decoration: BoxDecoration(
                    color:Color.fromRGBO(240, 86, 52, 1),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Center(child: StyledTextWidget(text: LocaleKeys.Add.tr(),size: 20,color: Colors.white)),)),

          ],
        ),
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return imageXFile==null?defaultScreen():itemsUploadFromScreen();
  }












}


