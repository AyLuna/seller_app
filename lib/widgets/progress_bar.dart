import 'package:flutter/material.dart';
circularProgress(){
  return Container(
    alignment:Alignment.center,
    child: Padding(
      padding: const EdgeInsets.all(12),
      child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(
          Colors.cyan
        ),
      ),
    ),
  );
}
linearProrgress(){
  return Container(
    alignment:Alignment.center,
    child: Padding(
      padding: const EdgeInsets.all(12),
      child: LinearProgressIndicator(
        valueColor: AlwaysStoppedAnimation(
            Colors.cyan
        ),
      ),
    ),
  );
}