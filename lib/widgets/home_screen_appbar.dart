import 'package:flutter/material.dart';

import '../global/global.dart';
import '../modal/items.dart';
import '../uploadScreens/items_upload_Screen.dart';
import 'styled_text_widget.dart';


class HomeScreenAppBar extends StatelessWidget with PreferredSizeWidget
{
  String? title;
  Items? model;
  final PreferredSizeWidget? bottom;

  HomeScreenAppBar({this.bottom, this.title, this.model});

  @override
  Size get preferredSize => bottom==null?Size(56, AppBar().preferredSize.height):Size(56, 80+AppBar().preferredSize.height);

  @override
  Widget build(BuildContext context)
  {
    return AppBar(
      iconTheme: IconThemeData(
          color:Color.fromRGBO(72, 72, 72, 1), size: 35
      ),
      flexibleSpace: Container(
        color: Colors.white,
      ),
      centerTitle: true,
      title: StyledTextWidget(text: "Bridge",size: 24,spacing: 3,weight:FontWeight.bold,color: Color.fromRGBO(240, 86, 52, 1),),
      actions: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (c)=>ItemsUploadScreen(model: model,)));
          }, icon: Icon(Icons.add_circle_outline_rounded,)),
        )
      ],
    );
  }
}

