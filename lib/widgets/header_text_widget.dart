import 'package:flutter/material.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';

class HeaderTextWidget extends SliverPersistentHeaderDelegate {
  final String? title;

  HeaderTextWidget(this.title);


  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return InkWell(onTap: (){},
        child: Container(
          height: 40,
          width: MediaQuery.of(context).size.width,
          child: InkWell(
            onTap: (){},
            child: Padding(
              padding: const EdgeInsets.fromLTRB(10, 4, 0, 0),
              child: StyledTextWidget(text:title!,size: 24,spacing: 2,weight: FontWeight.w500),
            ),
          ),
        ));
  }

  @override
  // TODO: implement maxExtent
  double get maxExtent => 50;

  @override
  // TODO: implement minExtent
  double get minExtent => 30;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate)=>true;
}
