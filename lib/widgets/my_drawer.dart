import 'package:easy_localization/easy_localization.dart';
import 'package:satyjy/widgets/language_picker.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';

import '../generated/locale_keys.g.dart';
import '../mainScreens/earnings_screen.dart';
import '../mainScreens/history_screen.dart';
import '../mainScreens/home_screen.dart';
import '../mainScreens/new_orders_screen.dart';
import 'package:flutter/material.dart';
import '../global/global.dart';

import '../authentication/auth_screen.dart';
class MyDrawer extends StatefulWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  State<MyDrawer> createState() => _MyDrawerState();
}


class _MyDrawerState extends State<MyDrawer> {




  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 25, bottom: 10),
            //header
            child: Container(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(1),
                    child: CircleAvatar(
                      radius: 100,
                      backgroundImage: NetworkImage(
                          sharedPreferences!.getString("photoUrl")!
                      ),
                    ),
                  ),
                  SizedBox(height: 10,),
                  StyledTextWidget(text: sharedPreferences!.getString("name")!,size: 20,),
                ],
              ),

            ),),
          //header

          //body drawer
          SizedBox(height:12),
          Container(
            child: Column(
              children: [
                Divider(height: 10,thickness: 2,color: Colors.grey,),
              ],
            ),
          ),
          ListTile(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (c)=>HomeScreen()));
            },
            leading: Icon(Icons.home, color: Colors.black,),
            title: StyledTextWidget(text: LocaleKeys.Home.tr(),size: 20,),
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          ListTile(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (c)=>NewOrdersScreen()));
            },
            leading: Icon(Icons.reorder, color: Colors.black,),
            title: StyledTextWidget(text: LocaleKeys.New_Orders.tr(),size: 20,),
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          ListTile(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (c)=>HistoryScreen()));
            },
            leading: Icon(Icons.local_shipping, color: Colors.black,),
            title: StyledTextWidget(text: LocaleKeys.Orders_History.tr(),size: 20,),
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          ListTile(
            onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (c)=>EarningsScreen()));
            },
            leading: Icon(Icons.monetization_on, color: Colors.black,),
            title: StyledTextWidget(text: LocaleKeys.My_earnings.tr(),size: 20,),
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          ListTile(
            onTap: (){
              firebaseAuth.signOut().then((value){
                Navigator.push(context, MaterialPageRoute(builder: (c)=>AuthScreen()));
              });
            },
            leading: Icon(Icons.exit_to_app, color: Colors.black,),
            title: StyledTextWidget(text: LocaleKeys.Sign_Out.tr(),size: 20,),
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              StyledTextWidget(text: LocaleKeys.Language.tr(),size: 20,maxLines: 3,),
              LanguagePicker(),
            ],
          ),
          const Divider(
            height: 10,
            color: Colors.grey,
            thickness: 2,
          ),
          //body drawer
        ],

      ),
    );
  }
}



