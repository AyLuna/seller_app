import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../mainScreens/home_screen.dart';
import '../modal/adress.dart';





class ShipmentAddressDesign extends StatelessWidget
{
  final Adress? model;
  final String? orderStatus;
  final String? orderId;
  final String? sellerId;
  final String? orderByUser;

  ShipmentAddressDesign({this.model, this.orderStatus, this.orderId, this.sellerId, this.orderByUser});



  @override
  Widget build(BuildContext context)
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 6.0,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 5),
          width: MediaQuery.of(context).size.width-10,
          child: Table(
            children: [
              TableRow(
                children: [
                  StyledTextWidget(text: LocaleKeys.Name.tr()),
                  StyledTextWidget(text: model!.name!),
                ],
              ),
              TableRow(
                children: [
                  StyledTextWidget(text: LocaleKeys.Phone.tr(),maxLines: 2,),
                  StyledTextWidget(text: model!.phoneNumber!),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: StyledTextWidget(text: model!.fullAdress!),
        ),


        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Center(
            child: InkWell(
              onTap: ()
              {
                Navigator.push(context, MaterialPageRoute(builder: (context) => const HomeScreen()));
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Color.fromRGBO(240, 86, 52, 1),
                  borderRadius: BorderRadius.circular(20),),
                width: MediaQuery.of(context).size.width - 70,
                height: 70,
                child: Center(
                  child: StyledTextWidget(text: orderStatus == "ended" ? LocaleKeys.Go_Back.tr(): LocaleKeys.Order_Packing_Done.tr(), color: Colors.white),
                ),
              ),
            ),
          ),
        ),
        const SizedBox(height: 20,),
      ],
    );
  }
}
