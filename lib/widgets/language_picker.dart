import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';

import '../global/global.dart';

class LanguagePicker extends StatefulWidget {
  const LanguagePicker({Key? key}) : super(key: key);

  @override
  State<LanguagePicker> createState() => _LanguagePickerState();
}

class _LanguagePickerState extends State<LanguagePicker> {
  final diller=["en","tm","ru"];


  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(7),
      padding: EdgeInsets.symmetric(horizontal: 7, vertical:4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
              color: Colors.black
          )
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton(
          items: diller.map((e)=>DropdownMenuItem<String>(
            child: StyledTextWidget(text: e,size: 20,),value:e,
          )).toList(),
          value: defaultDil,
          iconSize: 36,
          icon: Icon(Icons.arrow_drop_down_sharp,color:Colors.grey),
          onChanged: (value){
            setState(() {
              defaultDil=value.toString();
              context.setLocale(value=="en"?Locale('en'):value=="ru"?Locale('ru'):Locale('tr'));
            });
          },

        ),
      ),
    );
  }
}
