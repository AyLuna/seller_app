
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/item_detail_screen.dart';
import '../modal/items.dart';




class ItemsDesignWidget extends StatefulWidget
{
  Items? model;
  BuildContext? context;

  ItemsDesignWidget({this.model, this.context});

  @override
  _ItemsDesignWidgetState createState() => _ItemsDesignWidgetState();
}



class _ItemsDesignWidgetState extends State<ItemsDesignWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (c)=>ItemDetailsScreen(model:widget.model)));
      },
      splashColor: Colors.amber,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 2,
            blurRadius: 2,
          )],
          borderRadius: BorderRadius.circular(5),
        ),
        height: MediaQuery.of(context).size.height*0.35,
        width: MediaQuery.of(context).size.width*0.5-15,
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height*0.25,
              decoration: BoxDecoration(
                image: DecorationImage(image: NetworkImage(widget.model!.thumbnailUrl!),fit: BoxFit.cover),
                borderRadius: BorderRadius.circular(5),
              ),
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(child: StyledTextWidget(text: widget.model!.title!, color: Colors.cyan,size:16)),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(10, 2, 10, 2),
                    child: StyledTextWidget(text: widget.model!.longDescription!, maxLines: 1,size: 14,),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: StyledTextWidget(text: widget.model!.price!.toString()+" "+LocaleKeys.manat.tr(), color: Color.fromRGBO(240, 86, 52, 1),size: 14,),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
