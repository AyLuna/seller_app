import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import '../generated/locale_keys.g.dart';
import '../mainScreens/home_screen.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/error_dialog.dart';
import '../widgets/loading_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart' as fStorage;
import 'package:shared_preferences/shared_preferences.dart';

import '../global/global.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  XFile? imageXFile;
  final ImagePicker _picker = ImagePicker();
  String completeAdress="";

  Future<void> _getImage() async{
    imageXFile= await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      imageXFile;
    });
  }

  Position? position;
  List<Placemark>? placeMarks;

  String sellerImageUrl="";

  getCurrentLocation() async{
    Position newPosition = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    position=newPosition;

    placeMarks= await placemarkFromCoordinates(position!.latitude, position!.longitude);

    Placemark pmark=placeMarks![0];

    completeAdress='${pmark.subThoroughfare} ${pmark.thoroughfare}, ${pmark.subLocality} ${pmark.locality}, ${pmark.subAdministrativeArea}, ${pmark.administrativeArea}, ${pmark.postalCode}, ${pmark.country}';

    locationController.text=completeAdress;
  }

  Future<void> formValidation() async{
    if(imageXFile==null){
      showDialog(context: context, builder:(c){
        return ErrorDialog(message: LocaleKeys.Please_Upload_picture.tr(),);
      });
    }
    else{
      if(passwordController.text==confirmPasswordController.text){
        if(passwordController.text.isNotEmpty && confirmPasswordController.text.isNotEmpty && emailController.text.isNotEmpty && nameController.text.isNotEmpty && phoneController.text.isNotEmpty && locationController.text.isNotEmpty){
          showDialog(context: context, builder:(c){
                  return LoadingDialog(message: LocaleKeys.Registration_starts.tr(),);
              });
          String fileName=DateTime.now().microsecondsSinceEpoch.toString();
              fStorage.Reference reference=fStorage.FirebaseStorage.instance.ref().child("Sellers").child(fileName);
              fStorage.UploadTask uploadTask=reference.putFile(File(imageXFile!.path));
              fStorage.TaskSnapshot taskSnapshot= await uploadTask.whenComplete((){});
              await taskSnapshot.ref.getDownloadURL().then((url) {
                sellerImageUrl=url;

                authenticateSellesAndSignUp();
              });

        }
        else{
          showDialog(context: context, builder:(c){
                  return ErrorDialog(message: LocaleKeys.Please_fullfill_all_space.tr(),);
                });

        }
      }
      else{
        showDialog(context: context, builder:(c){
              return ErrorDialog(message: LocaleKeys.Passwords_are_not_equal.tr(),);
            });
      }
    }
  }



  void authenticateSellesAndSignUp() async{
    User? currentUser;


    await firebaseAuth.createUserWithEmailAndPassword(email: emailController.text.trim(), password: passwordController.text.trim()).then((auth) {
      currentUser=auth.user;
    }).catchError((error){
      Navigator.pop(context);
      showDialog(context: context, builder:(c){
        return ErrorDialog(message: error.message.toString(),);
      });

    });
    if(currentUser!=null){
      saveDataToFirebase(currentUser!).then((value) {
        Navigator.pop(context);

        Route newRoute=MaterialPageRoute(builder: (c)=>HomeScreen());
        Navigator.pushReplacement(context, newRoute);
      });
    }
  }

  Future saveDataToFirebase(User currentUser) async{
    FirebaseFirestore.instance.collection("sellers").doc(currentUser.uid).set({
      "sellerUID":currentUser.uid,
      "sellerEmail":currentUser.email,
      "sellerName":nameController.text.trim(),
      "sellerAvatarUrl":sellerImageUrl,
      "sellerPhone":phoneController.text.trim(),
      "adress":completeAdress,
      "status":"approved",
      "earnings":0.0,
      "lat":position!.latitude,
      "lng":position!.longitude,
      "language":defaultDil,

    });
    sharedPreferences=await SharedPreferences.getInstance();
    await sharedPreferences!.setString("uid", currentUser.uid);
    await sharedPreferences!.setString("email", currentUser.email.toString());
    await sharedPreferences!.setString("name", nameController.text.trim());
    await sharedPreferences!.setString("photoUrl", sellerImageUrl);
    await sharedPreferences!.setString("language", defaultDil);
  }

  Widget _username() {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(10),
      child: TextFormField(
        controller: phoneController,
        keyboardType: TextInputType.number,
        style: const TextStyle(color: Colors.blueGrey, fontSize: 16),
        decoration: InputDecoration(
          counterText: '',
          prefixIcon: const SizedBox(
            width: 60,
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Text(
                  '+993 ',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
          ),
          hintStyle: const TextStyle(color:Colors.grey),
          hintText: LocaleKeys.Phone.tr(),
          border: InputBorder.none,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
                color: Colors.grey,
                width: 2),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: Colors.grey,
                  width: 2)),
        ),
        validator: (value) {
          if (value!.isEmpty || value.trim().isEmpty) {
            return LocaleKeys.This_field_cannot_be_empty.tr();
          } else if (int.parse(value) < 60000000) {
            return LocaleKeys.Phone_number_must_be_between_60000000_and_65999999.tr();
          } else if (int.parse(value) > 65999999) {
            return LocaleKeys.Phone_number_must_be_between_60000000_and_65999999.tr();
          }
        },
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: MediaQuery.of(context).size.width - 40,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30), color: Colors.white),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  InkWell(
                    onTap: () {
                      _getImage();
                    },
                    child: CircleAvatar(
                      radius: MediaQuery.of(context).size.width * 0.20,
                      backgroundColor: Colors.grey.shade300,
                      backgroundImage: imageXFile == null
                          ? null
                          : FileImage(File(imageXFile!.path)),
                      child: imageXFile == null
                          ? Icon(
                        Icons.add_photo_alternate,
                        size: MediaQuery.of(context).size.width * 0.2,
                        color: Colors.white,
                      )
                          : null,
                    ),
                  ),
                  SizedBox(height: 10),
                  Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        CustomTextField(
                          data: Icons.person,
                          controller: nameController,
                          hintText: LocaleKeys.Name.tr(),
                          isObsecre: false,
                        ),
                        CustomTextField(
                          data: Icons.email,
                          controller: emailController,
                          hintText: LocaleKeys.Email.tr(),
                          isObsecre: false,
                        ),

                        _username(),

                        CustomTextField(
                          data: Icons.lock,
                          controller: passwordController,
                          hintText: LocaleKeys.Password.tr(),
                          isObsecre: true,
                        ),
                        CustomTextField(
                          data: Icons.lock,
                          controller: confirmPasswordController,
                          hintText: LocaleKeys.Confirm_Password.tr(),
                          isObsecre: true,
                        ),
                        CustomTextField(
                          data: Icons.phone,
                          controller: phoneController,
                          hintText: LocaleKeys.Phone.tr(),
                          isObsecre: false,
                        ),
                        CustomTextField(
                          data: Icons.my_location,
                          controller: locationController,
                          hintText: LocaleKeys.My_Current_Location.tr(),
                          isObsecre: false,
                          enabled: true,
                        ),
                        Container(
                          width: 400,
                          height: 40,
                          alignment: Alignment.center,
                          child: ElevatedButton.icon(
                            label: Text(
                              LocaleKeys.Get_my_Current_Location.tr(),
                              style: TextStyle(color: Colors.white),
                            ),
                            style: ElevatedButton.styleFrom(
                                primary: Colors.blue,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(30),
                                )),
                            onPressed: () => getCurrentLocation(),
                            icon: Icon(
                              Icons.location_on,
                              color: Colors.white,
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 12),
                  ElevatedButton(
                    child: Text(
                      LocaleKeys.Sign_Up.tr(),
                      style: TextStyle(color: Colors.white),
                    ),
                    style: ElevatedButton.styleFrom(
                        primary: Color.fromRGBO(240, 86, 52, 1),
                        padding:
                        EdgeInsets.symmetric(horizontal: 50, vertical: 10)),
                    onPressed: () => formValidation(),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),

          ],
        ),
    );
  }
}
