import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../mainScreens/home_screen.dart';
import '../authentication/auth_screen.dart';
import '../widgets/loading_dialog.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../widgets/custom_text_field.dart';
import '../widgets/error_dialog.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController phoneController = TextEditingController();


  formValidation() {
    if(emailController.text.isNotEmpty && passwordController.text.isNotEmpty){
      loginNow();
    }
    else{
      showDialog(context: context, builder:(c){
        return ErrorDialog(message: LocaleKeys.Please_write_email_and_password.tr(),);
      });

    }
  }


  loginNow() async {
    showDialog(context: context, builder:(c){
      return LoadingDialog(message: LocaleKeys.Checking_your_data.tr(),);
    });

    User? currentUser;
    await firebaseAuth.signInWithEmailAndPassword(email: emailController.text.trim(), password: passwordController.text.trim()).then((auth) {
      currentUser=auth.user!;
    }).catchError((error){
      Navigator.pop(context);
      showDialog(context: context, builder:(c){
        return ErrorDialog(message: error.message.toString(),);
      });
    });
    if(currentUser!=null){
      readDataAndSetDataLocally(currentUser!);
    }
  }


  Future readDataAndSetDataLocally(User currentUser) async{
    await FirebaseFirestore.instance.collection("sellers").doc(currentUser.uid).get().then((snapshot) async {
      if(snapshot.exists){
        await sharedPreferences!.setString("uid", snapshot.data()!["sellerUID"]);
        await sharedPreferences!.setString("email", snapshot.data()!["sellerEmail"]);
        await sharedPreferences!.setString("name", snapshot.data()!["sellerName"]);
        await sharedPreferences!.setString("photoUrl", snapshot.data()!["sellerAvatarUrl"]);
        await sharedPreferences!.setString("language", snapshot.data()!["language"]);
        Navigator.pop(context);
        Navigator.push(context, MaterialPageRoute(builder: (c)=>HomeScreen()));
      }
      else{
        firebaseAuth.signOut();
        Navigator.push(context, MaterialPageRoute(builder: (c)=>AuthScreen()));
        showDialog(context: context, builder:(c){
          return ErrorDialog(message: LocaleKeys.No_record_exists_Sign_Up_again.tr(),);
        });
      }
    });
  }

  Widget _username() {
    return Container(
      padding: const EdgeInsets.all(8),
      margin: const EdgeInsets.all(10),
      child: TextFormField(
        controller: phoneController,
        keyboardType: TextInputType.number,
        style: const TextStyle(color: Colors.blueGrey, fontSize: 16),
        decoration: InputDecoration(
          counterText: '',
          prefixIcon: const SizedBox(
            width: 60,
            child: Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(bottom: 5.0),
                child: Text(
                  '+993 ',
                  style: TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
              ),
            ),
          ),
          hintStyle: const TextStyle(color:Colors.grey),
          hintText: LocaleKeys.Phone.tr(),
          border: InputBorder.none,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
                color: Colors.grey,
                width: 2),
          ),
          focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide(color: Colors.grey,
                  width: 2)),
        ),
        validator: (value) {
          if (value!.isEmpty || value.trim().isEmpty) {
            return LocaleKeys.This_field_cannot_be_empty.tr();
          } else if (int.parse(value) < 60000000) {
            return LocaleKeys.Phone_number_must_be_between_60000000_and_65999999.tr();
          } else if (int.parse(value) > 65999999) {
            return LocaleKeys.Phone_number_must_be_between_60000000_and_65999999.tr();
          }
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.max ,
              children: [
                SizedBox(height: MediaQuery.of(context).size.height*.1,),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.all(15),
                    child: Image.asset("images/seller.png", height: MediaQuery.of(context).size.height/3,),
                  ),
                ),
                Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      CustomTextField(
                        data: Icons.email,
                        controller: emailController,
                        hintText: LocaleKeys.Email.tr(),
                        isObsecre: false ,
                      ),

                      _username(),

                      CustomTextField(
                        data: Icons.lock,
                        controller: passwordController,
                        hintText: LocaleKeys.Password.tr(),
                        isObsecre: true,
                      ),
                    ],
                  ),
                ),
                ElevatedButton(
                  child: Text(
                    LocaleKeys.Sign_In.tr(), style: TextStyle(color: Colors.white),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Colors.deepPurpleAccent,
                      padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10)
                  ),
                  onPressed:(){
                    formValidation();
                  },
                ),
                SizedBox(height:30),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

