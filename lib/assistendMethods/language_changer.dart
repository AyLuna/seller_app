import 'package:flutter/material.dart';
import '../global/global.dart';
class LanguageChanger extends ChangeNotifier{
  String _language= defaultDil;

  String get currentLanguage=>_language;


  changeCurrentLanguage() async {
    _language= defaultDil;
    notifyListeners();
  }

}