// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "Sign_Up": "Sign Up",
  "I_have_an_account_already": "I have an account already",
  "Sign_In": "Sign In",
  "Please_Upload_picture": "Please Upload picture",
  "Registration_starts": "Registration starts",
  "Please_fullfill_all_space": "Please fullfill all space",
  "Phone": "Phone",
  "This_field_cannot_be_empty": "This field can't be empty",
  "Phone_number_must_be_between_60000000_and_65999999": "Phone number must be between 60000000 and 65999999",
  "Name": "Name",
  "Email": "Email",
  "Password": "Password",
  "Confirm_Password": "Confirm Password",
    "My_Current_Location": "My Current Location",
  "Get_my_Current_Location": "Get my Current Location",
  "Please_write_email_and_password": "Please write email and password",
  "Please_write_phone_number_and_password": "Please write phone number and password",
  "Checking_your_data": "Checking your data ",
  "No_record_exists_Sign_Up_again ": "No record exists. Sign Up again",
  "You_earned": "You earned: ",
  "manat": " manat",
  "Back": "Back",
  "Orders_History": "Orders History",
  "Item_Deleted_Successfully": "Item Deleted Successfully.",
  "Delete_this_Item": "Delete this Item",
  "New_Orders": "New Orders",
  "Order_Id": "Order Id ",
  "Order_at": "Order at ",
  "Item_Image": "Item Image",
  "Capture_with_Camera": "Capture with Camera",
  "Select_from_Gallery": "Select from Gallery",
  "Cancel": "Cancel",
  "Please_add_Short_information_about_Item": "Please add Short information about Item",
  "Please_Add_picture_of_Item": "Please Add picture of Item",
  "Add_new_product": "Add new product",
  "Upload_new_product": "Upload new product",
  "Title": "Title",
  "Short_Info": "Short Info",
  "Price": "Price",
  "Add": "Add",
  "Please_wait": "\nPlease wait...",
  "Home": "Home",
  "My_earnings": "My earnings",
  "Sign_Out": "Sign Out",
  "Language": "Language",
  "Shipping_Details": "Shipping Details:",
  "Go_Back": "Go Back",
  "Order_Packing_Done": "Order Packing - Done",
  "Successful": "Successful",
  "Unsuccessful": "Unsuccessful",
  "Parcel_Delivered": "Parcel Delivered ",
  "Order_Placed ": "Order Placed ",
  "Passwords_are_not_equal": "Passwords are not equal",
    "Items":"Items",
    "Do_you_want_to_exit": "Do you want to exit?",
    "Yes": "Yes",
    "No": "No",
    "I_want_to_register": "I want to register"
};
static const Map<String,dynamic> ru = {
  "Sign_Up": "Регистрируйтесь",
  "I_have_an_account_already": "У меня есть аккаунт",
  "Sign_In": "Войти",
  "Please_Upload_picture": "Вставьте фотографию",
  "Registration_starts": "Регистрасия",
  "Please_fullfill_all_space": "Заполните все поля",
  "Phone": "Номер телефона",
  "This_field_cannot_be_empty": "Обязательно заполните это поля",
  "Phone_number_must_be_between_60000000_and_65999999": "Номер должен быть между 60000000 и 65999999",
  "Name": "Имя",
  "Email": "Почта",
  "Password": "Пароль",
  "Confirm_Password": "Потвердите пароль",
  "Passwords_are_not_equal": "Пароли не совпадают",
  "My_Current_Location": "Моё текущее местоположение",
  "Get_my_Current_Location": "Найдите текущее местоположение",
  "Please_write_email_and_password": "Введите почту и пароль",
  "Please_write_phone_number_and_password": "Введите номер и пароль",
  "Checking_your_data": "Проверка данных",
  "No_record_exists_Sign_Up_again": "Вы не были зарегистрированы ранее. Регистрируйтесь занова",
  "You_earned": "Ваш заработок",
  "manat": " манат",
  "Back": "Назад",
  "Orders_History": "История заказов",
  "Item_Deleted_Successfully": "Продукт удалён",
  "Items":"Продукты",
  "Delete_this_Item": "Удали этот продукт",
  "New_Orders": "Новые заказы",
  "Order_Id": "Номер заказа ",
  "Order_at": "Время заказа ",
  "Item_Image": "Фотография продукта",
  "Capture_with_Camera": "Сделай снимок с камеры",
  "Select_from_Gallery": "Выбери с галереи",
  "Cancel": "Отменить",
  "Please_add_Short_information_about_Item": "Добавьте описание продукта",
  "Please_Add_picture_of_Item": "Добавьте фотографию продукта",
  "Add_new_product": "Добав новый продукт",
  "Upload_new_product": "Продукт добавляется",
  "Title": "Название продукта",
  "Short_Info": "Описание",
  "Price": "Цена",
  "Add": "Добавит",
  "Please_wait": "\nПодождите немного",
  "Home": "Главное",
  "My_earnings": "Ваш заработок",
  "Sign_Out": "Выйти",
  "Language": "Поменяй язык",
  "Shipping_Details": "О заказе",
  "Go_Back": "Назад",
  "Order_Packing_Done": "Заказ отправлен",
  "Successful": "Выполнено",
  "Unsuccessful": "Не выполнено",
  "Parcel_Delivered": "Заказ ",
  "Order_Placed ": "Заказ ",
  "Do_you_want_to_exit": "Вы хотите выйти?",
  "Yes": "Да",
  "No": "Нет",
  "I_want_to_register": "Я новый пользователь"
};
static const Map<String,dynamic> tr = {
  "Sign_Up": "Ulgama gir",
  "I_have_an_account_already": "Men ulgama giripdim",
  "Sign_In": "Gir",
  "Please_Upload_picture": "Surat giriziň",
  "Registration_starts": "Hasaba alynýar",
  "Please_fullfill_all_space": "Öýjükleň baryny dolduruň",
  "Phone": "Telefon belgi",
  "This_field_cannot_be_empty": "Bul öýjük boş bolmaly däl",
  "Phone_number_must_be_between_60000000_and_65999999": "Nomer 60000000 we 65999999 sanlar arasynda bolmaly",
  "Name": "Ady",
  "Email": "Poçta",
  "Password": "Açar sözi",
  "Confirm_Password": "Açar sözi tassyklaň",
  "Passwords_are_not_equal": "Açar sözler deň däl",
  "My_Current_Location": "Häzirki ýerleşýän ýerim",
  "Get_my_Current_Location": "Häzirki ýerleşýän ýerimi tapyň",
  "Please_write_email_and_password": "Poçtaňyzy we açar sözüňizi ýazyň",
  "Please_write_phone_number_and_password": "Nomeriňizi we açar sözüňizi ýazyň",
  "Checking_your_data ": "Maglumatlar barlanylýar",
  "No_record_exists_Sign_Up_again": "Siz öň ulgama girmänsiňiz. Täzeden ulgama giriň",
  "You_earned": "Siziň gazanjyňyz",
  "manat": " manat",
  "Back": "Yza gaýt",
  "Orders_History": "Tabşyrylan sargytlar",
  "Item_Deleted_Successfully.": "Haryt pozuldy",
  "Items":"Harytlar",
  "Delete_this_Item": "Şu harydy poz",
  "New_Orders": "Täze sargytlar",
  "Order_Id": "Sargydyň belgisi ",
  "Order_at": "Sargydyň edilen wagty ",
  "Item_Image": "Harydyň surady",
  "Capture_with_Camera": "Kameradan surada al",
  "Select_from_Gallery": "Galereýadan al",
  "Cancel": "Goýbolsun et",
  "Please_add_Short_information_about_Item": "Haryt barada gysga maglumat beriň",
  "Please_Add_picture_of_Item": "Harydyň suratyny goşuň",
  "Add_new_product": "Täze haryt goşuň",
  "Upload_new_product": "Täze haryt goşulýar",
  "Title": "Harydyň ady",
  "Short_Info": "Gysga magumat",
  "Price": "Bahasy",
  "Add": "Goş",
  "Please_wait": "\nAzyrak garaşyň",
  "Home": "Baş sahypa",
  "My_earnings": "Gazanç ",
  "Sign_Out": "Ulgamdan çyk",
  "Language": "Dili üýtget",
  "Shipping_Details": "Sargyt hakynda",
  "Go_Back": "Yza gaýt",
  "Order_Packing_Done": "Sargyt Ugradyldy",
  "Successful": "tabşyryldy",
  "Unsuccessful": "tabşyrylmady",
  "Parcel_Delivered": "Sargyt ",
  "Order_Placed": "Sargyt ",
  "Do_you_want_to_exit": "Çykasyňyz gelýärmi?",
  "Yes": "Hawa",
  "No": "Ýok",
  "I_want_to_register": "Men täze ulanyjy"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "ru": ru, "tr": tr};
}
