import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import '../generated/locale_keys.g.dart';
import '../modal/items.dart';
import '../uploadScreens/items_upload_Screen.dart';
import '../widgets/home_screen_appbar.dart';
import '../widgets/items_design.dart';
import '../widgets/my_drawer.dart';
import '../widgets/progress_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../global/global.dart';
import 'package:satyjy/widgets/header_text_widget.dart';

import '../widgets/styled_text_widget.dart';
class HomeScreen extends StatefulWidget {
  final Items? model;

  const HomeScreen({Key? key, this.model}) : super(key: key);


  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        drawer: MyDrawer(),
        appBar: HomeScreenAppBar(),
        body: CustomScrollView(
          slivers: [
            SliverToBoxAdapter(
              child: Padding(
                padding: const EdgeInsets.only(left: 10,top: 10),
                child: StyledTextWidget(text: LocaleKeys.Items.tr(), size: 24,spacing: 2,weight: FontWeight.w500),
              ),
            ),
            StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance.collection("sellers").doc(sharedPreferences!.getString("uid")).collection("items").orderBy("publishedDate", descending: true).snapshots(),
                builder: (context, snapshot){
                  return !snapshot.hasData?SliverToBoxAdapter(
                  child: Center(child: circularProgress(),),
                  ):SliverStaggeredGrid.countBuilder(crossAxisCount: 2, staggeredTileBuilder: (c)=>StaggeredTile.fit(1), itemBuilder: (context, index){
                    Items model=Items.fromJson(
                      snapshot.data!.docs[index].data()! as Map<String, dynamic>,
                    );
                    return Padding(
                      padding: const EdgeInsets.only(left:10, right: 10, top: 10),
                      child: ItemsDesignWidget(
                        model: model,
                        context: context,
                      ),
                    );
                  }, itemCount: snapshot.data!.docs.length);
                }),
          ],
        ),
      ),
    );
  }
}
