import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:satyjy/mainScreens/home_screen.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';


class EarningsScreen extends StatefulWidget
{
  const EarningsScreen({Key? key}) : super(key: key);

  @override
  _EarningsScreenState createState() => _EarningsScreenState();
}




class _EarningsScreenState extends State<EarningsScreen>
{
  double sellerTotalEarnings = 0;

  retrieveSellerEarnings() async
  {
    await FirebaseFirestore.instance
        .collection("sellers")
        .doc(sharedPreferences!.getString("uid"))
        .get().then((snap)
    {
      setState(() {
        sellerTotalEarnings = double.parse(snap.data()!["earnings"].toString());
      });
    });
  }

  @override
  void initState() {
    super.initState();

    retrieveSellerEarnings();
  }

  @override
  Widget build(BuildContext context)
  {
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        body: SafeArea(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
              Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage("images/seller.png"),fit: BoxFit.fill),
      ),
      ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      StyledTextWidget(text: LocaleKeys.You_earned.tr(), size: 37,weight: FontWeight.bold),
                      StyledTextWidget(text: sellerTotalEarnings.toString()+LocaleKeys.manat.tr(), size: 37,weight: FontWeight.bold),
                    ],
                  ),
                  const SizedBox(height: 40.0,),

                  GestureDetector(
                    onTap: ()
                    {
                      Navigator.push(context, MaterialPageRoute(builder: (c)=> const HomeScreen()));
                    },
                    child: Container(
                      height: 70,
                      width: MediaQuery.of(context).size.width-70,
                      color: Color.fromRGBO(240, 86, 52, 1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.arrow_back_ios_new,
                            color: Colors.white, size: 27,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 13),
                            child: StyledTextWidget(text: LocaleKeys.Back.tr(), size:24,weight: FontWeight.bold, color: Colors.white,),
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
