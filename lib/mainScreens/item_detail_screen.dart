import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:satyjy/widgets/styled_text_widget.dart';
import '../generated/locale_keys.g.dart';
import '../global/global.dart';
import '../modal/items.dart';
import '../mainScreens/home_screen.dart';
import '../widgets/simple_app_bar.dart';


class ItemDetailsScreen extends StatefulWidget
{
  final Items? model;
  ItemDetailsScreen({this.model});

  @override
  _ItemDetailsScreenState createState() => _ItemDetailsScreenState();
}




class _ItemDetailsScreenState extends State<ItemDetailsScreen>
{
  TextEditingController counterTextEditingController = TextEditingController();


  deleteItem(String itemID)
  {
    FirebaseFirestore.instance
        .collection("sellers")
        .doc(sharedPreferences!.getString("uid"))
        .collection("items")
        .doc(itemID)
        .delete().then((value)
    {
      FirebaseFirestore.instance
          .collection("items")
          .doc(itemID)
          .delete();

      Navigator.push(context, MaterialPageRoute(builder: (c)=> const HomeScreen()));
      Fluttertoast.showToast(msg: LocaleKeys.Item_Deleted_Successfully.tr());
    });
  }

  @override
  Widget build(BuildContext context)
  {
    return WillPopScope(
      onWillPop: ()async =>false,
      child: Scaffold(
        appBar: SimpleAppBar(title: sharedPreferences!.getString("name"),),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: MediaQuery.of(context).size.height*0.45,
                decoration: BoxDecoration(
                  image: DecorationImage(image: NetworkImage(widget.model!.thumbnailUrl!),fit: BoxFit.cover),
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: Colors.grey),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:  StyledTextWidget(text: widget.model!.title.toString(), size: 22, weight: FontWeight.bold,),
              ),


              Padding(
                padding: const EdgeInsets.all(8.0),
                child: StyledTextWidget(text: widget.model!.longDescription.toString(), weight: FontWeight.bold, maxLines: 3,),
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: StyledTextWidget(text: widget.model!.price.toString() + LocaleKeys.manat.tr(), size: 30, weight: FontWeight.bold,),
              ),
              const SizedBox(height: 10,),
              Center(
                child: InkWell(
                  onTap: ()
                  {
                    //delete item
                    deleteItem(widget.model!.itemID!);
                  },
                  child: Container(
                    color: Colors.redAccent,
                    width: MediaQuery.of(context).size.width -70,
                    height: 70,
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          StyledTextWidget(text: LocaleKeys.Delete_this_Item.tr(), weight: FontWeight.bold, color: Colors.white),
                          Padding(
                            padding: const EdgeInsets.all(5),
                            child: Icon(Icons.delete_sweep, size: 27,color: Colors.white,),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
